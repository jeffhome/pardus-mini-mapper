// ==UserScript==
// @name        Pardus Mini Mapper
// @namespace   http://userscripts.xcom-alliance.info/
// @description Show a small map of the current sector on the navigation screen including your current position
// @include     http*://*.pardus.at/main.php*
// @version     1.7
// @author      Miche (Orion) / Sparkle (Artemis)
// @updateURL 	http://userscripts.xcom-alliance.info/mini_mapper/pardus_mini_mapper.meta.js
// @downloadURL http://userscripts.xcom-alliance.info/mini_mapper/pardus_mini_mapper.user.js
// @require		pardus_sector_data.js
// @require		pardus_wormhole_data.js
// @resource	settingsIcon settings_icon.gif
// @icon 		http://userscripts.xcom-alliance.info/mini_mapper/icon.png
// @grant		GM_getResourceURL
// @grant		GM_getValue
// @grant		GM_setValue
// ==/UserScript==

//
// Version Information
// -------------------
// v1.7 (19-Mar-2013)
//
//  - Fixed a broken WH link in FR 3-328
//
// v1.6 (05-Feb-2013)
//
//  - Fixed a broken WH tooltip in Greenso
//
// v1.5 (19-Oct-2012)
//
//  - Fixed maps not working correctly on certain sectors due to sector case mismatch
//  - Fixed incorrect wormhole destinations noticed in sectors with wormhole seals
//  - Added wormhole seal destinations to and from the Pardus sector
//  - Added destination coordinates to the WH tooltip when multiple WHs go to the same sector
//
// v1.4 (17-Oct-2012)
//
//  - Added another location below the Cargo/Other Ships section for the map display
//  - Show the coordinates of tiles as you mouse over them in the mini map
//  - Show the destination of wormholes in a tooltip when you mouse over one
//
// v1.3 (13-Oct-2012)
//
// - First public release announced in the Pardus forums
// - http://forum.pardus.at/index.php?showtopic=60205&view=findpost&p=1217105
//

var MiniSectorMap = {
	DEFAULT_DISPLAY_POSITION: 'left_ship',
	DEFAULT_TILE_SIZE: 5,
	MAP_WINDOW_WIDTH: 172,		// in pixels
	MAP_WINDOW_HEIGHT: 400,		// in pixels
	COLOUR_DATA: {
		'0':'#2250aa',	// 0: emax
		'1':'#113366',	// 1: energy
		'2':'#000000',	// 2: space
		'3':'#9A0D23',	// 3: nebula
		'4':'#777777',	// 4: asteroids
		'5':'#33FF00',	// 5: exotic matter
		'6':'#B8860B', 	// 6: planet
		'7':'#33FFEE',	// 7: wormhole (open)
		'8':'#FF0000',	// 8: wormhole (closed)
		'9':'#FFAA00',	// 9: xhole
		'a':'#DD33FF',	// a: yhole
		'b':'gold',		// b: wormhole seal
		'c':'#00EE88',	// c: viral cloud
		'd':'#4F647D',	// d: pardus station
		'e':'#4F6422'	// e: lucidi station
	},
	previousSector: '',
	DISPLAY_POSITION: GM_getValue(window.location.host.substr(0, window.location.host.indexOf('.')) + 'DisplayPosition', ''),
	TILE_SIZE: GM_getValue(window.location.host.substr(0, window.location.host.indexOf('.')) + 'TileSize', ''),
	SETTINGS_ICON: GM_getResourceURL('settingsIcon'),

	init: function(sectorData) {
		if (!sectorData || !sectorData.length) return;
		if (this.DISPLAY_POSITION == '') this.DISPLAY_POSITION = this.DEFAULT_DISPLAY_POSITION;
		if (this.TILE_SIZE == '') this.TILE_SIZE = this.DEFAULT_TILE_SIZE;
		this.TILE_SPACING = Math.floor(this.TILE_SIZE / 3);

		this.sectorData = sectorData;
		this.numRows = this.sectorData.length;
		this.numCols = String(this.sectorData[0]).length;
		this.mapWidth = (this.TILE_SIZE * this.numCols) + (this.TILE_SPACING * this.numCols - 1);
		this.mapHeight = (this.TILE_SIZE * this.numRows) + (this.TILE_SPACING * this.numRows - 1);

		this.els = {
			table: document.getElementById('yourship').cloneNode(true),
			window: document.createElement('div'),
			map: document.createElement('canvas'),
			mapcoords: document.createElement('span'),
			tooltip: document.createElement('span'),
			css: document.createElement('style')
		};

		// add in our css to the head of the page
		this.els.css.setAttribute('type', 'text/css');
		var css = '';
		css += '#miniMapper { position:relative; width:'+this.MAP_WINDOW_WIDTH+'px; height:'+this.MAP_WINDOW_HEIGHT+'px; overflow:hidden; }' + '\n';
		css += '#miniMapperMap { position:absolute; width:'+this.mapWidth+'px; height:'+this.mapHeight+'px; left:0px; top:0px; }' + '\n';
		css += '#miniMapperMap > div { position:absolute; width:'+this.TILE_SIZE+'px; height:'+this.TILE_SIZE+'px; }' + '\n';
		css += '#miniMapper .hilight { border: 1px solid white; margin: -1px 0px 0px -1px; }' + '\n';
		css += '#yourposition { position:absolute; }' + '\n';
		css += '#yourposition.left { left:205px;top:5px; }' + '\n';
		css += '#yourposition.right { right:205px;top:5px; }' + '\n';
		css += '#yourposition.left_ship { left:3px; }' + '\n';
		css += '#yourposition.right_ship { right:3px; }' + '\n';
		css += '#mini_map_settings { position:absolute;right:18px;top:7px;cursor:pointer; }' + '\n';
		css += '#mini_map_coords { position:absolute;left:18px;top:7px;color:#cce;font-size:11px; }' + '\n';
		css += '#minimaptooltip { position:absolute;background-color:#def;display:none;color:#000;font-size:9px;padding:2px 5px; }' + '\n';
		this.els.css.innerHTML = css;
		document.head.appendChild(this.els.css);

		// add in the sector mini map to the "window" div
		this.els.mapCtx = this.els.map.getContext('2d');
		if (!this.els.mapCtx) return;
		this.els.map.id = 'miniMapperMap';
		this.els.map.width = this.mapWidth;
		this.els.map.height = this.mapHeight;
		this.els.window.appendChild(this.els.map);
		this.els.map.addEventListener('mousemove', this.showtooltip.bind(this), true);
		this.els.map.addEventListener('mouseout', this.hidetooltip.bind(this), true);

		this.els.table.id = 'yourposition';
		this.els.table.className = this.DISPLAY_POSITION;
		this.els.table.rows[1].cells[0].innerHTML = '';
		this.els.table.rows[1].cells[0].align = 'center';
		this.els.table.rows[1].cells[0].style.textAlign = '';
		var img = this.els.table.rows[0].cells[0].getElementsByTagName('img')[0];
		img.removeAttribute('id');
		img.src = img.src.replace('ship.png', 'position.png');
		while (this.els.table.rows.length > 3) {
			this.els.table.deleteRow(-1);
		}

		// add the settings button
		var el = document.createElement('IMG');
		el.setAttribute('id','mini_map_settings');
		el.setAttribute('title','Mini Map Settings');
		el.src = this.SETTINGS_ICON;
		img.parentNode.setAttribute('style','position:relative;');
		img.parentNode.appendChild(el);
		el.addEventListener('click', this.clickSettingsButton.bind(this), true);

		// add the mini map mouseover coordinates placeholder
		this.els.mapcoords.setAttribute('id','mini_map_coords');
		img.parentNode.appendChild(this.els.mapcoords);

		// add the tooltip div to our cloned table
		this.els.tooltip.id = 'minimaptooltip';
		document.body.appendChild(this.els.tooltip);

		// add the "window" div to our cloned table
		this.els.window.id = 'miniMapper';
		this.els.table.rows[1].cells[0].appendChild(this.els.window);
		
	},
	
	render: function() {
		// If the map height is smaller than the window height, reduce the window height
		// If not, still set window dimensions in case it needs to grow again (e.g. after changing sector)
		var winWidth = Math.min(this.mapWidth, this.MAP_WINDOW_WIDTH);
		var winHeight = Math.min(this.mapHeight, this.MAP_WINDOW_HEIGHT);
		this.els.window.style.width = winWidth + 'px';
		this.els.window.style.height = winHeight + 'px';
		if (this.DISPLAY_POSITION == 'right_ship') {
			var container = document.getElementById('tdTabsRight');
		} else {
			var container = document.getElementById('tdTabsLeft');
		}
		if (!container) return;
		container.appendChild(this.els.table);
		var ctx = this.els.mapCtx;
		ctx.clearRect(0, 0, this.mapWidth, this.mapHeight);
		ctx.lineWidth = 1;
		ctx.strokeStyle = 'white';
		for (var row=0; row<this.numRows; row++) {
			for (var col=0; col<this.numCols; col++) {
				var tileData = String(this.sectorData[row])[col];
				var tileX = col * (this.TILE_SIZE + this.TILE_SPACING);
				var tileY = row * (this.TILE_SIZE + this.TILE_SPACING);
				ctx.fillStyle = this.COLOUR_DATA[tileData];
				ctx.fillRect(tileX, tileY, this.TILE_SIZE, this.TILE_SIZE);
			}
		}
		this.els.mapData = ctx.getImageData(0, 0, this.mapWidth, this.mapHeight);
		this.highlightCurrentTile();
	},

	showtooltip: function(evtData) {
		// calculate our minimap sector coordinates
		var _x = Math.floor( evtData.layerX / (this.TILE_SIZE + this.TILE_SPACING) );
		var _y = Math.floor( evtData.layerY / (this.TILE_SIZE + this.TILE_SPACING) );
		// show our minimap sector coordinates in the title of the map window
		this.els.mapcoords.innerHTML = '[' + _x + ',' + _y + ']';
		// if we have something to display for this coordinate, show the tooltip
		if (this.currentSector+'_'+_x+'_'+_y in wormholeData) {
			var _left = evtData.pageX - 10;
			var _top = evtData.pageY - 25;
			this.els.tooltip.style.left = _left + 'px';
			this.els.tooltip.style.top = _top + 'px';
			this.els.tooltip.innerHTML = wormholeData[this.currentSector+'_'+_x+'_'+_y];
			this.els.tooltip.style.display = 'block';
		} else {
			this.els.tooltip.innerHTML = '';
			this.els.tooltip.style.display = 'none';
		}
	},

	hidetooltip: function() {
		// hide map coordinates displayed in the title of the map window
		this.els.mapcoords.innerHTML = '';
		// hide the tooptip
		this.els.tooltip.style.display = 'none';
		this.els.tooltip.innerHTML = '';
	},

	clickSettingsButton: function(evtData) {
		var node = document.getElementById('MiniMapperSettingsSlide');
		if (!node) {
			// extra css for our settings popup
			var css = '';
			css += '#MiniMapperSettingsSlide { position:absolute;z-index:3;width:0;height:0;right:145px;top:5px; }' + '\n';
			css += '#yourposition.left_ship #MiniMapperSettingsSlide { left:10px; }' + '\n';
			css += '#yourposition.right_ship #MiniMapperSettingsSlide { right:305px; }' + '\n';
			css += '#MiniMapperSettingsSlide .tablepod { width:290px;border:2px ridge #a1a1af; }' + '\n';
			css += '#MiniMapperSettingsSlide .tablepod td { font-size:12px; }' + '\n';
			css += '#MiniMapperSettingsSlide .tablepod input[type=radio] { vertical-align:bottom; }' + '\n';
			css += '#MiniMapperSettingsSlide .tablepod .buttons { padding:8px 0;text-align:right;border-top:1px inset #9191a1; }' + '\n';
			css += '#MiniMapperSettingsSlide .tablepod .buttons button { border-width:1px;padding:2px 6px;margin:0 5px; }' + '\n';
			css += '#MiniMapperSettingsSlide .tablepod td em { color:#88a; }' + '\n';
			this.els.css.innerHTML += css;
			// the html for the settings form
			node = document.createElement('DIV');
			node.setAttribute('id','MiniMapperSettingsSlide');
			var _html = '';
					_html += '<table class="tablepod" style="background:url(' + unsafeWindow.imgDir + '/bgd.gif);" cellpadding="3">';
					_html += '<tr><th>Pardus MiniMapper Settings</th></tr>';
					_html += '<tr><td>';
					_html += '<div style="height:3px;"></div>';
					_html += '<em>Choose where the MiniMapper is shown...</em>';
					_html += '</td></tr>';
					_html += '<tr><td align="center">';
					_html += '<input type="radio" name="display_position" id="display_position_left" value="left"><label for="display_position_left">Left side of Nav</label> ';
					_html += '<input type="radio" name="display_position" id="display_position_right" value="right"><label for="display_position_right">Right side of Nav</label> ';
					_html += '<div style="height:3px;"></div>';
					_html += '<input type="radio" name="display_position" id="display_position_left_ship" value="left_ship"><label for="display_position_left_ship">Below Your Ship</label>';
					_html += '<input type="radio" name="display_position" id="display_position_right_ship" value="right_ship"><label for="display_position_right_ship">Below Other Ships</label>';
					_html += '</td></tr>';
					_html += '<tr><td>';
					_html += '<em>Choose the size of the MiniMapper tiles...</em>';
					_html += '</td></tr>';
					_html += '<tr><td align="center" style="padding-bottom:12px;">';
					_html += '<input type="radio" name="tile_size" id="tile_size_3"><label for="tile_size_3">Small</label> ';
					_html += '<input type="radio" name="tile_size" id="tile_size_5"><label for="tile_size_5">Medium</label> ';
					_html += '<input type="radio" name="tile_size" id="tile_size_6"><label for="tile_size_6">Large</label> ';
					_html += '<input type="radio" name="tile_size" id="tile_size_8"><label for="tile_size_8">Excessive</label>';
					_html += '</td></tr>';
					_html += '<tr><td class="buttons">';
					_html += '<button id="btn_close_minimapper_settings">Cancel</button>&nbsp;<button id="btn_save_minimapper_settings">Save and Close</button>';
					_html += '</td></tr>';
					_html += '</table>';
			node.innerHTML = _html;
			evtData.target.parentNode.appendChild(node);
			// attach click actions to the buttons
			document.getElementById('btn_close_minimapper_settings').addEventListener('click', this.clickCloseSettingsButton.bind(this), true);
			document.getElementById('btn_save_minimapper_settings').addEventListener('click', this.saveSettingsAndClose.bind(this), true);
		}
		// bring in our data
		this.displayCurrentSettingsData();
		// show the settings popup
		node.style.display = 'block';
	},

	displayCurrentSettingsData: function() {
		document.getElementById('display_position_' + this.DISPLAY_POSITION).checked = 'checked';
		document.getElementById('tile_size_' + this.TILE_SIZE).checked = 'checked';
	},

	clickCloseSettingsButton: function() {
		if (document.getElementById('MiniMapperSettingsSlide')) {
			document.getElementById('MiniMapperSettingsSlide').style.display = 'none';
		}
	},

	saveSettingsAndClose: function() {
		// display position
		if (document.getElementById('display_position_left').checked) {
			this.DISPLAY_POSITION = 'left';
		} else
		if (document.getElementById('display_position_right').checked) {
			this.DISPLAY_POSITION = 'right';
		} else
		if (document.getElementById('display_position_left_ship').checked) {
			this.DISPLAY_POSITION = 'left_ship';
		} else
		if (document.getElementById('display_position_right_ship').checked) {
			this.DISPLAY_POSITION = 'right_ship';
		} else {
			this.DISPLAY_POSITION = this.DEFAULT_DISPLAY_POSITION;
		}
		GM_setValue(window.location.host.substr(0, window.location.host.indexOf('.')) + 'DisplayPosition', this.DISPLAY_POSITION);
		// tile size
		if (document.getElementById('tile_size_3').checked) {
			this.TILE_SIZE = 3;
		} else
		if (document.getElementById('tile_size_5').checked) {
			this.TILE_SIZE = 5;
		} else
		if (document.getElementById('tile_size_6').checked) {
			this.TILE_SIZE = 6;
		} else
		if (document.getElementById('tile_size_8').checked) {
			this.TILE_SIZE = 8;
		} else {
			this.TILE_SIZE = this.DEFAULT_TILE_SIZE;
		}
		GM_setValue(window.location.host.substr(0, window.location.host.indexOf('.')) + 'TileSize', this.TILE_SIZE);
		// force redraw
		this.previousSector = "ForceUpdate";
		this.highlightCurrentTile();
		// close the settings window
		this.clickCloseSettingsButton();
	},

	getSector: function() {
		var sector = document.getElementById('sector');
		return sector ? sector.textContent : '';
	},

	highlightCurrentTile: function() {
		this.currentSector = this.getSector();
		if (this.previousSector != '' && this.previousSector != this.currentSector) {
			// Sector has changed since last render, re-initialise everything
			this.previousSector = this.currentSector;
			document.head.removeChild(this.els.css);					// remove old <style> block first
			this.els.table.parentNode.removeChild(this.els.table);		// remove old map elements first
			this.init(sectorData[this.currentSector]);
			this.render();
		} else {
			// Sector hasn't changed since last render
			var coords = null;
			var coordsEl = document.getElementById('coords');
			var coordsData = coordsEl.innerHTML.match(/\[(\d+?)\,(\d+)\]/);
			if (coordsData && coordsData.length && coordsData.length == 3) {
				coords = {};
				coords.x = parseInt(coordsData[1], 10);
				coords.y = parseInt(coordsData[2], 10);
			};
			if (!coords) return;
			// Unhighlight previous
			var ctx = this.els.mapCtx;
			ctx.putImageData(this.els.mapData, 0, 0);
			// Highlight current
			var tileX = coords.x * (this.TILE_SIZE + this.TILE_SPACING);
			var tileY = coords.y * (this.TILE_SIZE + this.TILE_SPACING);
			ctx.strokeRect(tileX-1, tileY-1, this.TILE_SIZE+2, this.TILE_SIZE+2);
			// Scroll current to centre of window (or as near to as possible)
			var centredTileX = (this.els.window.clientWidth / 2) - (this.TILE_SIZE / 2);
			var centredTileY = (this.els.window.clientHeight / 2) - (this.TILE_SIZE / 2);
			this.els.window.scrollLeft = tileX - centredTileX;
			this.els.window.scrollTop = tileY - centredTileY;
			this.previousSector = this.currentSector;
		}
	}
	
};

var sector = document.getElementById('sector');
if (sector) {
	var sector = sector.textContent;
	if (sector in sectorData) {
		MiniSectorMap.init(sectorData[sector]);
		MiniSectorMap.render();
		unsafeWindow.addUserFunction(function(msm) {
			return function() { msm.highlightCurrentTile() };
		}(MiniSectorMap));
	}
}
