// ==UserScript==
// @name        Pardus Mini Mapper
// @namespace   http://userscripts.xcom-alliance.info/
// @description Show a small map of the current sector on the navigation screen including your current position
// @include     http*://*.pardus.at/main.php*
// @version     1.7
// @author      Miche (Orion) / Sparkle (Artemis)
// @updateURL 	http://userscripts.xcom-alliance.info/mini_mapper/pardus_mini_mapper.meta.js
// @downloadURL http://userscripts.xcom-alliance.info/mini_mapper/pardus_mini_mapper.user.js
// @require		pardus_sector_data.js
// @require		pardus_wormhole_data.js
// @resource	settingsIcon settings_icon.gif
// @icon 		http://userscripts.xcom-alliance.info/mini_mapper/icon.png
// @grant		GM_getResourceURL
// @grant		GM_getValue
// @grant		GM_setValue
// ==/UserScript==
